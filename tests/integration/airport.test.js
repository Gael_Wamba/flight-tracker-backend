const request = require('supertest');
let server;

describe('/api/airports/', () => {
    beforeEach(() => {
        server = require('../../index');
    });

    afterEach(async () => {
        await server.close();
    });

    describe('GET /', () => {
        it('should return all airports', async () => {
            const res = await request(server).get('/api/airports/');
            expect(res.status).toBe(200);
            expect(res.body[0]).toHaveProperty('code');
            expect(res.body[0]).toHaveProperty('name');
        });

        it('should return 404 if endpoint was not found', async () => {
            const res = await request(server).get('/api/airport');
            expect(res.status).toBe(404);
        });
    });
});
