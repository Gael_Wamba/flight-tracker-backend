const request = require('supertest');

let server;
let departureDate;
let origin;
let destination;

describe('/api/flight-status/', () => {
    beforeEach(() => {
        server = require('../../index');
        departureDate = '2022-03-17'
        origin = 'DXB'
        destination = 'CPT'
    });

    afterEach(async () => {
        await server.close();
    });


    describe('GET /', () => {
        it('should return all flight-status', async () => {
            const res = await request(server).get('/api/flight-status?departureDate=' + departureDate + '&origin=' + origin + '&destination=' + destination);
            expect(res.status).toBe(200);
            expect(res.body[0]).toHaveProperty('flightNumber');
            expect(res.body[0]).toHaveProperty('flightRoute');
        });

        it('should return 400 if necessary params are not passed', async () => {
            const res = await request(server).get('/api/flight-status');
            expect(res.status).toBe(400);
            expect(res.text).toMatch(/Necessary params were not passed/);
        });

        it('should return 404 if endpoint was not found', async () => {
            const res = await request(server).get('/api/flight-statu?departureDate=' + departureDate + '&origin=' + origin + '&destination=' + destination);
            expect(res.status).toBe(404);
        });
    });
});
