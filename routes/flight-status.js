const express = require('express');
const router = express.Router();
const axios = require('axios');
const config = require('config');
const empty = require('../helpers/functions');

router.get('/', async (req, res) => {
    try {
        let { departureDate, origin, destination } = req.query;
        if (!empty(departureDate) && !empty(origin) && !empty(departureDate)) {
            let params = 'flight-status?departureDate=' + departureDate + '&origin=' + origin + '&destination=' + destination
            axios.get(config.get('baseURL') + params, {
                headers: { 'Access-Control-Allow-Origin': '*' }
            })
                .then((response) => {
                    res.send(response.data.results);
                })
                .catch((error) => {
                    res.status(400).send('Could not get results with given parameters: ' + error.response.data);
                })
        } else {
            res.status(400).send('Necessary params were not passed, (departureDate, origin, destination)');
        }

    } catch (error) {
        res.status(500).send("Something went wrong.  Don't worry it's not you and it's on our side")
    }
});

module.exports = router;

