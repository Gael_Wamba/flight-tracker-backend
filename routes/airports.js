const express = require('express');
const router = express.Router();
const axios = require('axios');
const config = require('config');

router.get('/', async (req, res) => {
    try {
        axios.get(config.get('baseURL') + 'airports?lang=en')
            .then((response) => {
                let airports = Object.values(response.data.results).map((airport) => {
                    return {
                        code: airport.iataCode,
                        name: airport.shortName + " (" + airport.iataCode + ")",
                    };
                });
                res.send(airports);
            })
            .catch((error) => {
                res.status(400).send(error.response.data)
            })
    } catch (error) {
        res.status(500).send("Something went wrong.  Don't worry it's not you and it's on our side")
    }
});
module.exports = router;

