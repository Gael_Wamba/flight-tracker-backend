const config = require('config');
const winston = require('winston');
require('express-async-errors');

module.exports = function () {
	//Catches uncaught exceptions and logs in with winston.
	winston.exceptions.handle(
		new winston.transports.Console({ 'timestamp': true }),
		new winston.transports.File({ filename: 'uncaughtException.log' })
	);

	// Catches unhandledRejection exceptions and logs in with winston.
	winston.add(new winston.transports.File({
		filename: 'log_file.log', format: winston.format.combine(
			winston.format.timestamp(),
			winston.format.json()
		)
	}));
};
