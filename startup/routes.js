const express = require('express');
const cors = require('cors');
const flightStatus = require('../routes/flight-status');
const airport = require('../routes/airports');
const error = require('../middleware/error');

module.exports = function (app) {
	app.use(express.json());
	app.use(cors())
	app.use('/api/flight-status', flightStatus);
	app.use('/api/airports', airport);

	//Always put the error handler middleware function at the end of every middlewares.
	app.use(error);
};
